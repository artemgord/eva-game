﻿using System.Collections.Generic;

namespace Source.Utilities
{
    public class ConcurrentQueue<T>
    {
        private readonly object _syncLock;
        private readonly Queue<T> _queue;

        public ConcurrentQueue()
        {
            this._queue = new Queue<T>();
            this._syncLock = new object();
        }
        
        public int Count
        {
            get
            {
                lock(this._syncLock) 
                {
                    return this._queue.Count;
                }
            }
        }

        public void Enqueue(T item)
        {
            lock(this._syncLock)
            {
                this._queue.Enqueue(item);
            }
        }

        public T Dequeue()
        {
            lock(this._syncLock)
            {
                return this._queue.Dequeue();
            }
        }
        
        public T Peek()
        {
            lock(this._syncLock)
            {
                return this._queue.Peek();
            }
        }
    }
}