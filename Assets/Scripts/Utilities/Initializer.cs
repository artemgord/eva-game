using Source.Actors;
using Source.Dispatchers;
using Source.Network;
using UnityEngine;

namespace Source.Utilities
{
    public class Initializer
    {
        //todo move to start application point
        public void InitializeInstances()
        {
            Debug.Log("Initialize Actor");
            var actor = Actor.Instance;
            Debug.Log("MessageDispatcher Actor");
            var messageDispatcher = MessageDispatcher.Instance;
            Debug.Log("NetworkClient Actor");
            var networkClient = NetworkClient.Instance;
        }
    }
    
    
}