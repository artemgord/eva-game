﻿using System;

namespace Source.Utilities
{
    public static class ValidationUtils
    {
        public static void NotNull(object obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException(obj.GetType().Name, $"{obj.GetType().Name} is null.");
            }
        }

        public static void NotNull(object[] obj)
        {
            foreach (var o in obj)
            {
                NotNull(o);
            }
        }
    }
}