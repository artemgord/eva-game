﻿using Source.Actors;
using Source.Network;
using Source.Network.Protocol;
using Source.Utilities;
using UnityEngine;
using UnityEngine.UI;

namespace Source.Contorllers.UI
{
    public class LoginController : MonoBehaviour
    {
        public GameObject LoginPanel;
        public GameObject LoginField;
        private string _login;
        private string _password;
        public GameObject PasswordField;
        public GameObject LoginButton;

        //public GameObject CharactersPanel;

        public void Start()
        {
            this._login = LoginField.GetComponent<InputField>().text;
            this._password = LoginField.GetComponent<InputField>().text;
            this.LoginButton.GetComponent<Button>().onClick.AddListener(this.OnLogin);
        }

        public void OnLogin()
        {
            string login = "vladislav";
            string password = "babydonthurtme";

            AuthorizationRequest authorizationRequest = new AuthorizationRequest(login, password);
            Actor.Instance.ProceedRequest<AuthorizationResponse>(authorizationRequest, response =>
            {
                Debug.Log(response);
                if (response.Result)
                {
                    LoginPanel.SetActive(false);
                    CharactersController.InitCharacters(response.UserCharacters);
                   // CharactersPanel.SetActive(true);
                }

                //todo from tests login in first player
                ShortPlayerMessage responseUserCharacter = response.UserCharacters[0];
                LoginRequest loginRequest = new LoginRequest(responseUserCharacter.PlayerId);
                Actor.Instance.ProceedRequest<LoginResponse>(loginRequest, message =>
                {
                    //todo in the world
                });
            });
        }
    }
}