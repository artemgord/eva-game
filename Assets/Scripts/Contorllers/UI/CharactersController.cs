﻿using System;
using System.Collections.Generic;
using Source.Actors;
using Source.Dispatchers;
using Source.Network;
using Source.Network.Protocol;
using UnityEngine;
using UnityEngine.UI;

namespace Source.Contorllers.UI
{
	public class CharactersController : MonoBehaviour
	{
		public GameObject CharactersPanel;
		public GameObject ContentPanel;
		public GameObject CharacterButtonPrefab;
		

		[SerializeField]
		private static List<ShortPlayerMessage> _characters;
		public static void InitCharacters(List<ShortPlayerMessage> characters)
		{
			_characters = characters;
//			_characters.Add(characters[0]);
//			_characters.Add(characters[0]);
//			_characters.Add(characters[0]);
//			_characters.Add(characters[0]);
//			_characters.Add(characters[0]);
//			_characters.Add(characters[0]);
//			_characters.Add(characters[0]);
//			_characters.Add(characters[0]);
//			_characters.Add(characters[0]);
		
		}
		
		public void Start()
		{
			if (_characters != null)
			{
				foreach (var character in _characters)
				{
					GameObject newCharacter = Instantiate(CharacterButtonPrefab) as GameObject;
					CharacterListItemController controller = newCharacter.GetComponent<CharacterListItemController>();

					controller.NameLabel.text += character.Name;
					controller.LevelLabel.text += character.Level.ToString();
					controller.WinsStatLabel.text += character.PlayerStatsMessage.WinsCount.ToString();
					controller.LosesStatLabel.text += character.PlayerStatsMessage.LosesCount.ToString();
					controller.DrawsStatLabel.text += character.PlayerStatsMessage.DrawsCount.ToString();
					controller.EscapesStatLabel.text += character.PlayerStatsMessage.EscapesCount.ToString();
					controller.WinsInARowStatLabel.text += character.PlayerStatsMessage.WinsInRowCount.ToString();

					newCharacter.transform.parent = ContentPanel.transform;
					newCharacter.transform.localScale = Vector3.one;
					newCharacter.active = true;
				}
				//todo set size of content panel for correct scrolling
			}
			
		}
			

		
	}
}
