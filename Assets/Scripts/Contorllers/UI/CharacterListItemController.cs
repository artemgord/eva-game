﻿using UnityEngine;
using UnityEngine.UI;

namespace Source.Contorllers.UI
{
    public class CharacterListItemController : MonoBehaviour
    {
        public Image CharacterImage;
        public Text NameLabel;
        public Text LevelLabel;
        public Text WinsStatLabel;
        public Text LosesStatLabel;
        public Text DrawsStatLabel;
        public Text EscapesStatLabel;
        public Text WinsInARowStatLabel;
        
    }
}