﻿using System;
using System.Timers;
using Assets.Scripts.UI.Screens.LoginScreen;
using Source.Actors;
using Source.Dispatchers;
using Source.Network;
using Source.Network.Protocol;
using Source.Utilities;
using UnityEngine;
using Object = System.Object;
using Ping = Source.Network.Protocol.Ping;

namespace Source.Contorllers
{
    public class PingPongController : IDisposable
    {
        private readonly int _interval;
        private readonly Timer _timer;
        private bool _disposed;
        private PingScreen screen;
        private long lastRequestTime = 0;
        public long ping { get; set; }
        public long delay { get; set; }

        public PingPongController(PingScreen pingScreen)
        {
            this.screen = pingScreen;
            this._interval = Configuration.PingInterval;
            this._timer = new Timer(_interval)
            {
                AutoReset = true
            };
            this._timer.Elapsed += SendPing;

            MessageDispatcher.Instance.Subscribe(MessageType.Ping, this.OnPing);
            MessageDispatcher.Instance.Subscribe(MessageType.Pong, this.OnPong);
        }

        public void Start()
        {
            this._timer.Start();
        }

        public void Stop()
        {
            this._timer.Stop();
        }

        private void OnPing(Message message)
        {
            Ping ping = message as Ping;
            if (ping != null)
            {
                Pong pong = new Pong(TimeUtils.CurrentTimeMillis());
                var instance = Actor.Instance;
                instance.ProcessMessage(pong);
            }
        }

        private void OnPong(Message message)
        {
            Pong pong = message as Pong;
            if (pong != null)
            {
                ping = TimeUtils.CurrentTimeMillis() - lastRequestTime;
                delay = pong.Time - lastRequestTime;
                screen.ShowPingStatistic(ping, delay);
            }
        }

        private void SendPing(Object source, ElapsedEventArgs e)
        {
            lastRequestTime = TimeUtils.CurrentTimeMillis();
            Ping ping = new Ping(lastRequestTime);
            Actor.Instance.ProcessMessage(ping);
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (this._disposed) return;

            if (disposing)
            {
                this._timer?.Dispose();
                MessageDispatcher.Instance.Unsubscribe(MessageType.Ping, this.OnPing);
                MessageDispatcher.Instance.Unsubscribe(MessageType.Pong, this.OnPong);
            }

            this._disposed = true;
        }

        ~PingPongController()
        {
            this.Dispose(false);
        }
    }
}
