﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Source.Gamer
{
    public class UnitChanger : MonoBehaviour
    {
        public List<Sprite> listOfUnits;
        public Image image;
        private int countUnits;

        private void Start()
        {
            listOfUnits = new List<Sprite>();
            countUnits = listOfUnits.Count;
        }

        public void Next()
        {
            for (int i = 0; i <= countUnits; i++)
            {
                image.sprite = listOfUnits[i];
            }
        }

        private void AddAllUnits()
        {

        }

        void Previous()
        {

        }
    }
}
