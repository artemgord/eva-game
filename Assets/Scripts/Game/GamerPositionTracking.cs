﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

public class GamerPositionTracking : MonoBehaviour
{
    public GameObject gamer;
    private float transformPositionX;
    private float transformPositionY;
    private Rigidbody rigidbody;
   

    void Start()
    {
        gamer = GameObject.FindGameObjectWithTag("Player");
        rigidbody = gamer.GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        transformPositionX = gamer.transform.position.x;
        transformPositionY = gamer.transform.position.y;

        if (rigidbody.velocity.magnitude == 0)
        {
            Debug.Log("Stop");
            return;
        }

        Debug.Log("X = " + transformPositionX + "\nY = " + transformPositionY);
    }
}
