﻿using System;
using System.Collections.Generic;
using Source.Network;
using Source.Network.Protocol;
using UnityEngine;

namespace Source.Actors
{
    public class Actor : MonoBehaviour
    {
        private static Actor _instance;
        private long _messageCounter = 0;
        private readonly Dictionary<long, IFuture<RequestResponseMessage>> _callbacksMap;

        public Actor()
        {
            _callbacksMap = new Dictionary<long, IFuture<RequestResponseMessage>>();
        }

        public static Actor Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = FindObjectOfType<Actor>();
                    if (_instance == null)
                    {
                        GameObject gameObject = new GameObject { name = "Actor" };
                        _instance = gameObject.AddComponent<Actor>();
                        DontDestroyOnLoad(gameObject);
                    }
                }
                return _instance;
            }
        }
        public void ProcessMessage(Message message)
        {
            NetworkClient.Instance.AddToSendQueue(message);
        }
        
        public IFuture<T> ProceedRequest<T>(RequestResponseMessage message, ResponseHandler<T> callback) where T: RequestResponseMessage
        {
            long requestId = _messageCounter++;
            message.Metadata.RequestId = requestId;
            
            IFuture<T> future = new Future<T>();
            future.AddListener(callback);
            _callbacksMap.Add(requestId, future);

            NetworkClient.Instance.AddToSendQueue(message);

            return future;
        }

        public void ProceedResponse(RequestResponseMessage message)
        {
            long responseId = message.Metadata.ResponseId;

            IFuture<RequestResponseMessage> future = _callbacksMap[responseId];
            future.SetResult(message);

            try
            {
                future.Notify();
            }
            catch (Exception e)
            {
                Debug.LogError(e.Message);
            }
            finally
            {
                _callbacksMap.Remove(responseId);
            }
        }
    }
}