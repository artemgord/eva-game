﻿using Source.Network.Protocol;

namespace Source.Actors
{
    public class Future<T>: IFuture<T> where T : RequestResponseMessage
    {
        private ResponseHandler<T> _callback;

        private T _result;

        public void SetResult(RequestResponseMessage message)
        {
            this._result = message as T;
        }

        public void AddListener(ResponseHandler<T> callback)
        {
            this._callback = callback;
        }

        public void Notify()
        {
            _callback.Invoke(_result);
        }
    }
}