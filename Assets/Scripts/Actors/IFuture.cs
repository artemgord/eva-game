﻿using Source.Network.Protocol;

namespace Source.Actors
{
    public interface IFuture<out T> where T: RequestResponseMessage
    {
        void SetResult(RequestResponseMessage message);

        void AddListener(ResponseHandler<T> callback);

        void Notify();
    }
}