﻿using Source.Network.Protocol;

namespace Source.Actors
{
    public delegate void ResponseHandler<in T>(T message) where T : RequestResponseMessage;
}