﻿using Source.Actors;
using Source.Network.Protocol;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Events;

namespace Source.Dispatchers
{
    public class UnityMessageEvent<T> : UnityEvent<T> where T: Message
    {
    }

    public class MessageDispatcher: MonoBehaviour
    {
        private static MessageDispatcher _instance;

        private readonly Dictionary<MessageType, UnityEvent<Message>> _eventDictionary;

        public MessageDispatcher()
        {
            this._eventDictionary = new Dictionary<MessageType, UnityEvent<Message>>();
        }

        public static MessageDispatcher Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = FindObjectOfType<MessageDispatcher>();
                    if (_instance == null)
                    {
                        GameObject gameObject = new GameObject { name = "MessageDispatcher" };
                        _instance = gameObject.AddComponent<MessageDispatcher>();
                        DontDestroyOnLoad(gameObject);
                    }
                }
                return _instance;
            }
        }

        public void ProceedMessage(Message message)
        {
            if (!message.IsRequestResponseMessage())
            {
                this.NotifyListeners(message);
            }
            else
            {
                Actor.Instance.ProceedResponse(message as RequestResponseMessage);
            }
        }

        public void Subscribe(MessageType type, UnityAction<Message> callback)
        {
            UnityEvent<Message> unityEvent;
            if (!this._eventDictionary.TryGetValue(type, out unityEvent))
            {
                unityEvent = new UnityMessageEvent<Message>();
                this._eventDictionary.Add(type, unityEvent);
            }

            unityEvent.AddListener(callback);
        }

        public void Unsubscribe(MessageType type, UnityAction<Message> callback)
        {
            UnityEvent<Message> unityEvent;
            if (this._eventDictionary.TryGetValue(type, out unityEvent))
            {
                unityEvent.RemoveListener(callback);
            }
        }

        private void NotifyListeners(Message message)
        {
            UnityEvent<Message> unityEvent;
            if (this._eventDictionary.TryGetValue(message.Type, out unityEvent))
            {
                unityEvent.Invoke(message);
            }
        }
    }
}