﻿namespace Source.Network
{
    public static class Configuration
    {
        public static readonly string ServerIpAddress;
        public static readonly int ServerPort;
        public static readonly int PingInterval;

        static Configuration()
        {
            ServerPort = 8998;
            ServerIpAddress = "159.69.7.101";
            PingInterval = 1000;
        }
    }
}
