using FlatBuffers;
using Source.Network.FlatProtocol;

namespace Source.Network.Protocol
{
    public class Ping : Message
    {
        public long Time { get; set; }

        public Ping() 
        {
            this.Type = MessageType.Ping;
        }

        public Ping(long time)
        {
            this.Time = time;
            this.Type = MessageType.Ping;
        }

        public override byte[] GetAsBytes() 
        {
            FlatBufferBuilder builder = new FlatBufferBuilder(1);
            Offset<FlatPing> pingOffset = FlatPing.CreateFlatPing(builder, this.Time);
            Offset<FlatMessage> messageOffset = FlatMessage.CreateFlatMessage(builder, FlatMessageObject.FlatPing, pingOffset.Value);
            builder.Finish(messageOffset.Value);
            return builder.SizedByteArray();
        }

        public override int GetOffset(FlatBufferBuilder builder)
        {
            throw new System.NotImplementedException();
        }

        public override Metadata GetMetadata()
        {
            return null;
        }

        public override string ToString()
        {
            return $"Ping: ({nameof(Time)}: {Time})";
        }
    }
}
