﻿namespace Source.Network.Protocol
{
    public enum Side
    {
        DARK = 0,
        LIGHT = 1,
        APOSTATE = 2
    }
}