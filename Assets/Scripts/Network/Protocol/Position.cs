﻿using FlatBuffers;
using Source.Network.FlatProtocol;
using Source.Network.Protocol;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Network.Protocol
{
    class Position : Message
    {
        private float Position_x { get; set; }
        private float Position_y { get; set; }
        private float Position_z { get; set; }

        public Position()
        {
            this.Type = MessageType.Position;
        }

        public override byte[] GetAsBytes()
        {
            FlatBufferBuilder flatBufferBuilder = new FlatBufferBuilder(1);
            Offset<FlatPosition> offset = FlatPosition.CreateFlatPosition(flatBufferBuilder,
                Position_x,
                Position_y,
                Position_z);
            flatBufferBuilder.Finish(offset.Value);
            return flatBufferBuilder.SizedByteArray();
        }

        public override int GetOffset(FlatBufferBuilder builder)
        {
            return 0;
        }

        public override Metadata GetMetadata()
        {
            return null;
        }

        public override string ToString()
        {
            return $"X = ({nameof(Position_x)} {Position_x})" + "\n" + $"Y = ({nameof(Position_y)} {Position_y})";
        }

    }
}
