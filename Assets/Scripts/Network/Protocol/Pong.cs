using FlatBuffers;
using Source.Network.FlatProtocol;

namespace Source.Network.Protocol
{
    public class Pong : Message
    {
        public long Time { get; set; }

        public Pong() 
        {
            this.Type = MessageType.Pong;
        }
        
        public Pong(long time)
        {
            this.Time = time;
            this.Type = MessageType.Pong;
        }
        public override byte[] GetAsBytes() 
        {
            FlatBufferBuilder builder = new FlatBufferBuilder(1);
            Offset<FlatPong> pongOffset = FlatPong.CreateFlatPong(builder, this.Time);
            Offset<FlatMessage> messageOffset = FlatMessage.CreateFlatMessage(builder, FlatMessageObject.FlatPong, pongOffset.Value);
            builder.Finish(messageOffset.Value);
            return builder.SizedByteArray();
        }

        public override int GetOffset(FlatBufferBuilder builder)
        {
            throw new System.NotImplementedException();
        }

        public override Metadata GetMetadata()
        {
            return null;
        }

        public override string ToString()
        {
            return $"Pong: ({nameof(Time)}: {Time})";
        }
    }
}
