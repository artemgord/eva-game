﻿using System;
using FlatBuffers;
using Source.Network.FlatProtocol;
using Source.Utilities;

namespace Source.Network.Protocol
{
    public class ShortPlayerMessage : Message
    {
        public EUID PlayerId { get; set; }

        public String Name { get; set; }

        public byte Level { get; set; }

        public Side Side { get; set; }
        
        public bool HasCube { get; set; }
        
        public Guild Guild { get; set; }
        
        public byte UnitsInArmy { get; set; }
        
        public PlayerStatsMessage PlayerStatsMessage { get; set; }

        public ShortPlayerMessage()
        {
            this.Type = MessageType.ShortPlayerMessage;
        }

        public ShortPlayerMessage(EUID playerId, string name, byte level, Side side, bool hasCube, Guild guild, byte unitsInArmy, PlayerStatsMessage playerStatsMessage)
        {
            ValidationUtils.NotNull(playerStatsMessage);
            this.PlayerId = playerId;
            this.Name = name;
            this.Level = level;
            this.Side = side;
            this.HasCube = hasCube;
            this.Guild = guild;
            this.UnitsInArmy = unitsInArmy;
            this.PlayerStatsMessage = playerStatsMessage;
            this.Type = MessageType.ShortPlayerMessage;
        }

        public override byte[] GetAsBytes() 
        {
            FlatBufferBuilder builder = new FlatBufferBuilder(1);
            Offset<FlatShortPlayerMessage> offset = new Offset<FlatShortPlayerMessage>(this.GetOffset(builder));
            builder.Finish(offset.Value);
            return builder.SizedByteArray();
        }

        public override int GetOffset(FlatBufferBuilder builder)
        {
            Offset<FlatShortPlayerMessage> shortPlayerMessageOffset = FlatShortPlayerMessage.CreateFlatShortPlayerMessage(
                builder,
                new Offset<FlatUID>(this.PlayerId.GetOffset(builder)),
                builder.CreateString(this.Name),
                (sbyte) this.Level,
                (FlatSide) Enum.Parse(typeof(FlatSide), this.Side.ToString()),
                this.HasCube,
                new Offset<FlatGuildObject>(this.Guild.GetOffset(builder)),
                (sbyte) this.UnitsInArmy,
                 new Offset<FlatPlayerStatsMessage>(this.PlayerStatsMessage.GetOffset(builder))
            );

            Offset<FlatMessage> messageOffset = FlatMessage.CreateFlatMessage(
                builder,
                FlatMessageObject.FlatShortPlayerMessage,
                shortPlayerMessageOffset.Value
            );

            return messageOffset.Value;
        }

        public override Metadata GetMetadata()
        {
            return null;
        }

        public override string ToString()
        {
            return $"ShortPlayerMessage: ({nameof(PlayerId)}: {PlayerId}, {nameof(Name)}: {Name}, {nameof(Level)}: {Level}, {nameof(Side)}: {Side}, {nameof(HasCube)}: {HasCube}, {nameof(Guild)}: {Guild}, {nameof(UnitsInArmy)}: {UnitsInArmy}, {nameof(PlayerStatsMessage)}: {PlayerStatsMessage})";
        }
    }
}