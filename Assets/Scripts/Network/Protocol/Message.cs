using FlatBuffers;

namespace Source.Network.Protocol
{
    public abstract class Message
    {
        public MessageType Type { get; set; }

        public bool IsRequestResponseMessage()
        {
            return this.GetMetadata() != null;
        }

        public abstract byte[] GetAsBytes();

        public abstract int GetOffset(FlatBufferBuilder builder);

        public abstract Metadata GetMetadata();
    }
}
