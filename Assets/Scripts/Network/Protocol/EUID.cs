using System;
using FlatBuffers;
using Source.Network.FlatProtocol;

namespace Source.Network.Protocol
{
    public class EUID : Message
    {
        public Guid Id { get; set; }

        public EUID()
        {
            this.Type = MessageType.EUID;
        }

        public EUID(byte[] bytes)
        {
            this.Id = new Guid(bytes);
            this.Type = MessageType.EUID;
        }

        public override byte[] GetAsBytes() 
        {
            FlatBufferBuilder builder = new FlatBufferBuilder(1);
            Offset<FlatUID> offset = new Offset<FlatUID>(this.GetOffset(builder));
            builder.Finish(offset.Value);
            return builder.SizedByteArray();
        }

        public override int GetOffset(FlatBufferBuilder builder)
        {
            byte[] guidBytes = this.Id.ToByteArray();

            builder.StartVector(8, 16, 0);
            foreach (byte guidByte in guidBytes)
            {
                builder.AddByte(guidByte);
            }

            VectorOffset guidOffset = builder.EndVector();
            
            Offset<FlatUID> uidOffset = FlatUID.CreateFlatUID(
                builder,
                guidOffset
            );

            Offset<FlatMessage> messageOffset = FlatMessage.CreateFlatMessage(
                builder, 
                FlatMessageObject.FlatUID,
                uidOffset.Value
            );

            return messageOffset.Value;
        }

        public override Metadata GetMetadata()
        {
            return null;
        }

        public override string ToString()
        {
            return $"EUID: ({nameof(Id)}: {Id})";
        }
    }
}
