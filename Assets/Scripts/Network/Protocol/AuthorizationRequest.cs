using FlatBuffers;
using Source.Network.FlatProtocol;
using Source.Utilities;

namespace Source.Network.Protocol
{
    public class AuthorizationRequest : RequestResponseMessage
    {
        public string Login { get; set; }

        public string Password { get; set; }

        public AuthorizationRequest() 
        {
            this.Type = MessageType.AuthorizationRequest;
        }
        
        public AuthorizationRequest(string login, string password)
        {
            this.Metadata = new Metadata();
            this.Login = login;
            this.Password = password;
            this.Type = MessageType.AuthorizationRequest;
        }

        public AuthorizationRequest(Metadata metadata, string login, string password)
        {
            ValidationUtils.NotNull(metadata);
            this.Metadata = metadata;
            this.Login = login;
            this.Password = password;
        }

        public override byte[] GetAsBytes()
        {
            FlatBufferBuilder builder = new FlatBufferBuilder(1);
            Offset<FlatAuthorizationRequest> offset = new Offset<FlatAuthorizationRequest>(this.GetOffset(builder));
            builder.Finish(offset.Value);
            return builder.SizedByteArray();
        }

        public override int GetOffset(FlatBufferBuilder builder)
        {
            StringOffset loginOffset = builder.CreateString(Login);
            StringOffset passwordOffset = builder.CreateString(Password);
            Offset<FlatAuthorizationRequest> authorizationRequestOffset =
                FlatAuthorizationRequest.CreateFlatAuthorizationRequest(
                    builder,
                        new Offset<FlatMetadata>(this.Metadata.GetOffset(builder)), loginOffset, passwordOffset);
            Offset<FlatMessage> flatMessageOffset = FlatMessage.CreateFlatMessage(
                builder,
                    FlatMessageObject.FlatAuthorizationRequest, authorizationRequestOffset.Value);
            return flatMessageOffset.Value;
        }

        public override Metadata GetMetadata()
        {
            return this.Metadata;
        }

        public override string ToString()
        {
            return $"AuthorizationRequest: ({nameof(Metadata)}: {Metadata}, {nameof(Login)}: {Login}, {nameof(Password)}: {Password})";
        }
    }
}
