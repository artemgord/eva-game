﻿using FlatBuffers;
using Source.Network.FlatProtocol;

namespace Source.Network.Protocol
{
    public class PlayerStatsMessage : Message
    {
        public EUID PlayerId { get; set; }
        
        public long WinsCount { get; set; }

        public long LosesCount { get; set; }
        
        public long DrawsCount { get; set; }
        
        public long EscapesCount { get; set; }
        
        public long WinsInRowCount { get; set; }
        

        public PlayerStatsMessage()
        {
            this.Type = MessageType.PlayerStatsMessage;
        }
        
        public PlayerStatsMessage(EUID playerId, long winsCount, long losesCount, long drawsCount, long escapesCount, long winsInRowCount)
        {
            this.PlayerId = playerId;
            this.WinsCount = winsCount;
            this.LosesCount = losesCount;
            this.DrawsCount = drawsCount;
            this.EscapesCount = escapesCount;
            this.WinsInRowCount = winsInRowCount;
            this.Type = MessageType.PlayerStatsMessage;
        }

        public override byte[] GetAsBytes() 
        {
            FlatBufferBuilder builder = new FlatBufferBuilder(1);
            Offset<FlatPlayerStatsMessage> offset = new Offset<FlatPlayerStatsMessage>(this.GetOffset(builder));
            builder.Finish(offset.Value);
            return builder.SizedByteArray();
        }

        public override int GetOffset(FlatBufferBuilder builder)
        {
            Offset<FlatPlayerStatsMessage> playerStatsMessageOffset = FlatPlayerStatsMessage.CreateFlatPlayerStatsMessage(
                builder,
                new Offset<FlatUID>(this.PlayerId.GetOffset(builder)),
                this.WinsCount,
                this.LosesCount,
                this.DrawsCount,
                this.EscapesCount,
                this.WinsInRowCount
            );

            Offset<FlatMessage> messageOffset = FlatMessage.CreateFlatMessage(
                builder,
                FlatMessageObject.FlatPlayerStatsMessage,
                playerStatsMessageOffset.Value
            );

            return messageOffset.Value;
        }

        public override Metadata GetMetadata()
        {
            return null;
        }

        public override string ToString()
        {
            return $"PlayerStatsMessage: ({nameof(PlayerId)}: {PlayerId}, {nameof(WinsCount)}: {WinsCount}, {nameof(LosesCount)}: {LosesCount}, {nameof(DrawsCount)}: {DrawsCount}, {nameof(EscapesCount)}: {EscapesCount}, {nameof(WinsInRowCount)}: {WinsInRowCount})";
        }
    }
}