using System.Collections.Generic;
using FlatBuffers;
using Source.Network.FlatProtocol;
using Source.Utilities;

namespace Source.Network.Protocol
{
    public class AuthorizationResponse : RequestResponseMessage
    {
        public bool Result { get; set; }

        public List<ShortPlayerMessage> UserCharacters { get; set; }

        public AuthorizationResponse()
        {
            this.Type = MessageType.AuthorizationResponse;
        }

        public AuthorizationResponse(bool result, List<ShortPlayerMessage> userCharacters)
        {
            ValidationUtils.NotNull(userCharacters);
            this.Metadata = new Metadata();
            this.Result = result;
            this.UserCharacters = userCharacters;
            this.Type = MessageType.AuthorizationResponse;
        }

        public AuthorizationResponse(Metadata metadata, bool result, List<ShortPlayerMessage> userCharacters)
        {
            ValidationUtils.NotNull(new object[] { metadata, userCharacters });
            this.Metadata = metadata;
            this.Result = result;
            this.UserCharacters = userCharacters;
        }

        public override byte[] GetAsBytes() 
        {
            FlatBufferBuilder builder = new FlatBufferBuilder(1);
            Offset<FlatAuthorizationResponse> offset = new Offset<FlatAuthorizationResponse>(this.GetOffset(builder));
            builder.Finish(offset.Value);
            return builder.SizedByteArray();
        }

        public override int GetOffset(FlatBufferBuilder builder)
        {
            Offset<FlatShortPlayerMessage>[] arrayOffsets = new Offset<FlatShortPlayerMessage>[this.UserCharacters.Count];
            for (int i = 0; i < this.UserCharacters.Count; ++i)
            {
                arrayOffsets[i] = new Offset<FlatShortPlayerMessage>(this.UserCharacters[i].GetOffset(builder));
            }

            VectorOffset userCharsOffset = builder.CreateVectorOfTables(arrayOffsets);
            Offset<FlatAuthorizationResponse> authorizationResponseOffset =
                   FlatAuthorizationResponse.CreateFlatAuthorizationResponse(
                       builder,
                       new Offset<FlatMetadata>(this.Metadata.GetOffset(builder)),
                       this.Result,
                       userCharsOffset);

            Offset<FlatMessage> messageOffset =
                FlatMessage.CreateFlatMessage(
                    builder,
                    FlatMessageObject.FlatAuthorizationResponse, authorizationResponseOffset.Value);

            return messageOffset.Value;
        }

        public override Metadata GetMetadata()
        {
            return this.Metadata;
        }

        public override string ToString()
        {
            return $"AuthorizationResponse: ({nameof(Metadata)}: {Metadata}, {nameof(Result)}: {Result}, {nameof(UserCharacters)}: {UserCharacters})";
        }
    }
}
