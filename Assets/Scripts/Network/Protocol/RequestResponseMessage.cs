﻿namespace Source.Network.Protocol
{
    public abstract class RequestResponseMessage: Message
    {
        public Metadata Metadata { get; set; }
    }
}