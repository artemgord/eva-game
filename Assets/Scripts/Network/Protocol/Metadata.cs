using FlatBuffers;
using Source.Network.FlatProtocol;

namespace Source.Network.Protocol
{
    public class Metadata : Message
    {
        public long RequestId { get; set; }

        public long ResponseId { get; set; }

        public Metadata()
        {
            this.Type = MessageType.Metadata;
        }

        public Metadata(long requestID, long responseID)
        {
            this.RequestId = requestID;
            this.ResponseId = responseID;
            this.Type = MessageType.Metadata;
        }
        
        public override byte[] GetAsBytes() 
        {
            FlatBufferBuilder builder = new FlatBufferBuilder(1);
            Offset<FlatMetadata> metadataOffset = FlatMetadata.CreateFlatMetadata(builder, this.RequestId, this.ResponseId);
            Offset<FlatMessage> messageOffset = FlatMessage.CreateFlatMessage(builder, FlatMessageObject.FlatMetadata, metadataOffset.Value);
            builder.Finish(messageOffset.Value);
            return builder.SizedByteArray();
        }

        public override int GetOffset(FlatBufferBuilder builder)
        {
            return FlatMetadata.CreateFlatMetadata(builder, this.RequestId, this.ResponseId).Value;
        }

        public override Metadata GetMetadata()
        {
            return null;
        }

        public override string ToString()
        {
            return $"Metadata: ({nameof(RequestId)}: {RequestId}, {nameof(ResponseId)}: {ResponseId})";
        }
    }
}
