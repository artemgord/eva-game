using System;
using FlatBuffers;
using Source.Network.FlatProtocol;
using Source.Utilities;

namespace Source.Network.Protocol
{
    public class LoginRequest : RequestResponseMessage
    {
        public EUID PlayerId { get; set; }
        
        public EUID SessionId { get; set; }
        
        public SessionState SessionState { get; set; }

        public LoginRequest()
        {
            this.Type = MessageType.LoginRequest;
        }
        
        public LoginRequest(EUID playerId)
        {
            this.Metadata = new Metadata();
            this.PlayerId = playerId;
            this.Type = MessageType.LoginRequest;
        }

        public LoginRequest(Metadata metadata, EUID playerId, SessionState sessionState)
        {
            ValidationUtils.NotNull(metadata);
            this.Metadata = metadata;
            this.PlayerId = playerId;
            this.SessionState = sessionState;
        }

        public override byte[] GetAsBytes() 
        {
            FlatBufferBuilder builder = new FlatBufferBuilder(1);
            Offset<FlatLoginRequest> offset = new Offset<FlatLoginRequest>(this.GetOffset(builder));
            builder.Finish(offset.Value);
            return builder.SizedByteArray();
        }

        public override int GetOffset(FlatBufferBuilder builder)
        {
            Offset<FlatLoginRequest> loginRequestOffset = FlatLoginRequest.CreateFlatLoginRequest(
                builder,
                new Offset<FlatMetadata>(this.Metadata.GetOffset(builder)),
                new Offset<FlatUID>(this.PlayerId.GetOffset(builder)),
                new Offset<FlatUID>(this.SessionId.GetOffset(builder)),
                (FlatSessionState) Enum.Parse(typeof(FlatSessionState), this.SessionState.ToString())
            );

            Offset<FlatMessage> messageOffset = FlatMessage.CreateFlatMessage(
                builder,
                FlatMessageObject.FlatLoginRequest,
                loginRequestOffset.Value
            );

            return messageOffset.Value;
        }

        public override Metadata GetMetadata()
        {
            return this.Metadata;
        }

        public override string ToString()
        {
            return $"LoginRequest: ({nameof(Metadata)}: {Metadata}, {nameof(PlayerId)}: {PlayerId}, {nameof(SessionState)}: {SessionState})";
        }
    }
}
