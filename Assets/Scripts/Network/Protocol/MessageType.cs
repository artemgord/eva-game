﻿namespace Source.Network.Protocol
{
    public enum MessageType
    {
        None,
        Ping,
        Pong,
        EUID,
        Metadata,
        GuildObject,
        AuthorizationResponse,
        AuthorizationRequest,
        LoginRequest,
        LoginResponse,
        ShortPlayerMessage,
        PlayerStatsMessage,
        Position,
        CreateGeoObject,
        UpdateGeoObjectPosition,
        UpdateMyPosition,
        DisposeGeoObject,
        PlanetInfo
    }
}