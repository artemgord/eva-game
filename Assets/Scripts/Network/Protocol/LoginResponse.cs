using FlatBuffers;
using Source.Network.FlatProtocol;
using Source.Utilities;

namespace Source.Network.Protocol
{
    public class LoginResponse : RequestResponseMessage
    {
        public bool Success { get; set; }

        public LoginResponse()
        {
            this.Type = MessageType.LoginResponse;
        }

        public LoginResponse(bool success)
        {
            this.Metadata = new Metadata();
            this.Success = success;
            this.Type = MessageType.LoginResponse;
        }

        public LoginResponse(Metadata metadata, bool success)
        {
            ValidationUtils.NotNull(metadata);
            this.Metadata = metadata;
            this.Success = success;
        }

        public override byte[] GetAsBytes() 
        {
            FlatBufferBuilder builder = new FlatBufferBuilder(1);
            Offset<FlatLoginResponse> offset = new Offset<FlatLoginResponse>(this.GetOffset(builder));
            builder.Finish(offset.Value);
            return builder.SizedByteArray();
        }

        public override int GetOffset(FlatBufferBuilder builder)
        {
            Offset<FlatLoginResponse> loginResponseOffset = FlatLoginResponse.CreateFlatLoginResponse(
                builder,
                new Offset<FlatMetadata>(this.Metadata.GetOffset(builder)),
                this.Success
            );

            Offset<FlatMessage> messageOffset = FlatMessage.CreateFlatMessage(
                builder,
                FlatMessageObject.FlatLoginResponse
            );

            return messageOffset.Value;
        }

        public override Metadata GetMetadata()
        {
            return this.Metadata;
        }

        public override string ToString()
        {
            return $"LoginResponse: ({nameof(Metadata)}: {Metadata.ToString()}, {nameof(Success)}: {Success})";
        }
    }
}
