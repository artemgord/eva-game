﻿using System;
using FlatBuffers;
using Source.Network.FlatProtocol;

namespace Source.Network.Protocol
{
    public class Guild : Message
    {
        public GuildType GuildType { get; set; }

        public byte GuildLevel { get; set; }

        public Guild()
        {
            this.Type = MessageType.GuildObject;
        }
        
        public Guild(GuildType guildType, byte guildLevel)
        {
            GuildType = guildType;
            GuildLevel = guildLevel;
            this.Type = MessageType.GuildObject;
        }
        
        public override byte[] GetAsBytes()
        {
            FlatBufferBuilder builder = new FlatBufferBuilder(1);
            Offset<FlatGuild> offset = new Offset<FlatGuild>(this.GetOffset(builder));
            builder.Finish(offset.Value);
            return builder.SizedByteArray();
        }

        public override int GetOffset(FlatBufferBuilder builder)
        {
            Offset<FlatGuildObject> guildOffset = FlatGuildObject.CreateFlatGuildObject(
                builder,
                (FlatGuild) Enum.Parse(typeof(FlatSide), this.GuildType.ToString())
            );

            Offset<FlatMessage> messageOffset = FlatMessage.CreateFlatMessage(
                builder,
                FlatMessageObject.FlatGuildObject,
                guildOffset.Value
            );

            return messageOffset.Value;
        }

        public override Metadata GetMetadata()
        {
            return null;
        }

        public override string ToString()
        {
            return $"Guild: ({nameof(GuildType)}: {GuildType}, {nameof(GuildLevel)}: {GuildLevel})";
        }
    }
}