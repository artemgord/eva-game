﻿namespace Source.Network.Protocol
{
    public enum GuildType
    {
        HUNTER= 0,
        MINER = 1,
        INVADER = 2,
        DEFENDER = 3,
        SUPREME = 4,
        HERO = 5
    }
}