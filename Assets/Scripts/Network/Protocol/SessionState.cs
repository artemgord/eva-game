﻿namespace Source.Network.Protocol
{
    public enum SessionState
    {
        RESET = 0,
        CONTINUE = 1,
        CLOSE = 2
    }
}