// <auto-generated>
//  automatically generated by the FlatBuffers compiler, do not modify
// </auto-generated>

namespace Source.Network.FlatProtocol
{

using global::System;
using global::FlatBuffers;

public struct FlatPing : IFlatbufferObject
{
  private Table __p;
  public ByteBuffer ByteBuffer { get { return __p.bb; } }
  public static FlatPing GetRootAsFlatPing(ByteBuffer _bb) { return GetRootAsFlatPing(_bb, new FlatPing()); }
  public static FlatPing GetRootAsFlatPing(ByteBuffer _bb, FlatPing obj) { return (obj.__assign(_bb.GetInt(_bb.Position) + _bb.Position, _bb)); }
  public void __init(int _i, ByteBuffer _bb) { __p.bb_pos = _i; __p.bb = _bb; }
  public FlatPing __assign(int _i, ByteBuffer _bb) { __init(_i, _bb); return this; }

  public long Time { get { int o = __p.__offset(4); return o != 0 ? __p.bb.GetLong(o + __p.bb_pos) : (long)0; } }

  public static Offset<FlatPing> CreateFlatPing(FlatBufferBuilder builder,
      long time = 0) {
    builder.StartObject(1);
    FlatPing.AddTime(builder, time);
    return FlatPing.EndFlatPing(builder);
  }

  public static void StartFlatPing(FlatBufferBuilder builder) { builder.StartObject(1); }
  public static void AddTime(FlatBufferBuilder builder, long time) { builder.AddLong(0, time, 0); }
  public static Offset<FlatPing> EndFlatPing(FlatBufferBuilder builder) {
    int o = builder.EndObject();
    return new Offset<FlatPing>(o);
  }
};


}
