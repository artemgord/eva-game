﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Net.WebSockets;
using System.Threading;
using System.Threading.Tasks;
using FlatBuffers;
using Source.Contorllers;
using Source.Dispatchers;
using Source.Network.FlatProtocol;
using Source.Utilities;
using UnityEngine;
using UnityEngine.UI;
using Message = Source.Network.Protocol.Message;

namespace Source.Network
{
    public class NetworkClient : MonoBehaviour
    {
        private static NetworkClient _instance;

        private Thread _sendThread;
        private Thread _recieveThread;
        private ConcurrentQueue<Message> _sendMessageQueue;
        private ConcurrentQueue<Message> _receiveMessageQueue;
        public bool IsConnected;

        // #TODO: reset counter if > max long
        public long RequestsCount;
        private ClientWebSocket _clientWebSocket;
        private Task _receive;

        public void Start()
        {
            this.StartConnection();

            // ReSharper disable once InconsistentlySynchronizedField
            this._sendMessageQueue = new ConcurrentQueue<Message>();
            // ReSharper disable once InconsistentlySynchronizedField
            this._receiveMessageQueue = new ConcurrentQueue<Message>();
            this.StartThreads();
        }

        public static NetworkClient Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = FindObjectOfType<NetworkClient>();
                    if (_instance == null)
                    {
                        GameObject gameObject = new GameObject {name = "NetworkClient"};
                        _instance = gameObject.AddComponent<NetworkClient>();
                        DontDestroyOnLoad(gameObject);
                    }
                }

                return _instance;
            }
        }

        void Awake()
        {
            if (_instance == null)
            {
                _instance = this;
                DontDestroyOnLoad(this.gameObject);
            }
            else
            {
                Destroy(gameObject);
            }
        }

        public void Update()
        {
            if (this.IsConnected)
            {
                if (this._receiveMessageQueue.Count > 0)
                {
                    MessageDispatcher.Instance.ProceedMessage(this._receiveMessageQueue.Dequeue());
                }
            }
            else
            {
                Reconnect();
                Debug.Log("Not connected");
            }
        }

        private void Reconnect()
        {
            if (this.IsConnected)
            {
                // TODO: show message for reconnect
            }
        }

        public void AddToSendQueue(Message message)
        {
            if (message != null)
            {
                if (message.IsRequestResponseMessage())
                {
                    message.GetMetadata().RequestId = this.RequestsCount++;
                }

                this._sendMessageQueue.Enqueue(message);
            }
            else
            {
                throw new ArgumentNullException(nameof(message), "Message could not be null.");
            }
        }

        public void OnApplicationQuit()
        {
            this.IsConnected = false;
//            this._stream.Close();
//            this._clientWebSocket.CloseAsync()();
        }

        private void StartConnection()
        {
            this.CloseConnection();
            _clientWebSocket = new ClientWebSocket();

            try
            {
                Uri uri = new Uri("ws://" + Configuration.ServerIpAddress + ":" + Configuration.ServerPort);
                var connectAsync = _clientWebSocket.ConnectAsync(
                    uri,
                    CancellationToken.None);

                connectAsync.Wait();
                this.IsConnected = true;
            }
            catch (Exception e)
            {
                Debug.Log("Socket was not created");
            }
        }

        private void StartThreads()
        {
            if (this.IsConnected)
            {
                this._sendThread = new Thread(this.Send)
                {
                    IsBackground = true
                };

                this._sendThread.Start();
                Debug.Log("NetworkClient started sending thread.");
                
                this._recieveThread = new Thread(this.Receive)
                {
                    IsBackground = true
                };
                _recieveThread.Start();
                Debug.Log("NetworkClient started received thread.");
            }
        }

        private void Receive()
        {
            System.Threading.Thread.Sleep(1000);
            while (true)
            {
                Task.Run(ReceiveTask).Wait();
                System.Threading.Thread.Sleep(16);
            }
        }

        private void Send()
        {
            while (true)
            {
                if (this._sendMessageQueue.Count > 0)
                {
                    Message message = this._sendMessageQueue.Peek();
                    Debug.Log($" --> : {Configuration.ServerIpAddress}:{Configuration.ServerPort} --> {message.ToString()}");

                    byte[] payload = message.GetAsBytes();

                    try
                    {
                        var arraySegment = new ArraySegment<byte>(payload);
                        this._clientWebSocket.SendAsync(arraySegment, WebSocketMessageType.Binary, true, CancellationToken.None);
                        this._sendMessageQueue.Dequeue();
                    }
                    catch (Exception e)
                    {
                        Debug.Log($"Send failed: {e}");
                        this.IsConnected = false;
                    }
                }
                else
                {
                    System.Threading.Thread.Sleep(16);
                }
            }
        }

        private async Task ReceiveTask()
        {
            try
            {
                var buffer = new ArraySegment<byte>(new byte[1024]);
                WebSocketReceiveResult result = null;
                var allBytes = new List<byte>();

                do
                {
                    result = await _clientWebSocket.ReceiveAsync(buffer, CancellationToken.None);
                    for (int i = 0; i < result.Count; i++)
                    {
                        allBytes.Add(buffer.Array[i]);
                    }
                } while (!result.EndOfMessage);


                FlatMessage flatMessage = this.GetMessageFromBytes(allBytes.ToArray());

                Message message = MessageFactory.MessageFactory.CreateMessage(flatMessage);
                Debug.Log($" <-- : {Configuration.ServerIpAddress}:{Configuration.ServerPort} <-- {message.ToString()}");
                // no one else should enqueue to this queue
                this._receiveMessageQueue.Enqueue(message);
            }
            catch (Exception e)
            {
                Debug.Log(e);
                throw;
            }
        }

        private FlatMessage GetMessageFromBytes(byte[] data)
        {
            ByteBuffer byteBuffer = new ByteBuffer(data);
            return FlatMessage.GetRootAsFlatMessage(byteBuffer);
        }

        private void CloseConnection()
        {
//            if (this._stream != null)
//            {
//                this._stream.Close();
//                this._stream = null;
//            }
//
//            if (this._client != null)
//            {
//                this._client.Close();
//                this._client = null;
//            }
        }
    }
}