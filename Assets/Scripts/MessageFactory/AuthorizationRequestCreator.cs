﻿using Source.Network.FlatProtocol;
using Source.Network.Protocol;

namespace Source.MessageFactory
{
    public static partial class MessageFactory
    {
        private static AuthorizationRequest CreateAuthorizationRequestMessage(FlatMessage flatMessage)
        {
            FlatAuthorizationRequest? flatAuthorizationRequest = flatMessage.MessageObject<FlatAuthorizationRequest>();

            return new AuthorizationRequest(
                CreateMetadataMessage(flatAuthorizationRequest.Value.Metadata.Value),
                flatAuthorizationRequest.Value.Login,
                flatAuthorizationRequest.Value.Password
            );
        }
    }
}