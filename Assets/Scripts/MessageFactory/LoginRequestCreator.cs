﻿using System;
using Source.Network.FlatProtocol;
using Source.Network.Protocol;

namespace Source.MessageFactory
{
    public static partial class MessageFactory
    {
        private static LoginRequest CreateLoginRequest(FlatMessage flatMessage)
        {
            FlatLoginRequest? flatLoginRequest = flatMessage.MessageObject<FlatLoginRequest>();
            return new LoginRequest(
                CreateMetadataMessage(flatLoginRequest.Value.Metadata.Value),
                CreateEUID(flatLoginRequest.Value.PlayerID.Value),
                (SessionState) Enum.Parse(typeof(SessionState), flatLoginRequest.Value.SessionState.ToString())
            );
        }
    }
}