﻿using Source.Network.FlatProtocol;
using Source.Network.Protocol;

namespace Source.MessageFactory
{
    public static partial class MessageFactory
    {
        private static LoginResponse CreateLoginResponse(FlatMessage flatMessage)
        {
            FlatLoginResponse? flatLoginResponse = flatMessage.MessageObject<FlatLoginResponse>();
            return new LoginResponse(
                CreateMetadataMessage(flatLoginResponse.Value.Metadata.Value),
                flatLoginResponse.Value.Success
            );
        }
    }
}