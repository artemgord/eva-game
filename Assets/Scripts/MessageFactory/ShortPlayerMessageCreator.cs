﻿using System;
using Source.Network.FlatProtocol;
using Source.Network.Protocol;

namespace Source.MessageFactory
{
    public static partial class MessageFactory
    {
        private static ShortPlayerMessage CreateShortPlayerMessage(FlatShortPlayerMessage flatShortPlayerMessage)
        {
            return new ShortPlayerMessage(
                CreateEUID(flatShortPlayerMessage.PlayerID.Value),
                flatShortPlayerMessage.Name,
                (byte) flatShortPlayerMessage.Level,
                (Side) Enum.Parse(typeof(Side), flatShortPlayerMessage.Side.ToString()),
                flatShortPlayerMessage.Cube,
                CreateGuildMessage(flatShortPlayerMessage.Guild.Value),
                (byte) flatShortPlayerMessage.UnitsInArmyCount,
                CreatePlayerStatsMessage(flatShortPlayerMessage.PlayerStats.Value)
            );
        }
    }
}