﻿using Source.Network.FlatProtocol;
using Source.Network.Protocol;

namespace Source.MessageFactory
{
    public static partial class MessageFactory
    {
        private static Metadata CreateMetadataMessage(FlatMetadata flatMetadata)
        {
            return new Metadata(flatMetadata.RequestID, flatMetadata.ResponseID);
        }
    }
}