﻿using Source.Network.FlatProtocol;
using Source.Network.Protocol;

namespace Source.MessageFactory
{
    public static partial class MessageFactory
    {
        private static EUID CreateEUID(FlatUID flatUID)
        {
            byte[] euidBytes = new byte[flatUID.BytesLength];
            for (int i = 0; i < euidBytes.Length; ++i)
            {
                euidBytes[i] = (byte) flatUID.Bytes(i);
            }

            return new EUID(euidBytes);
        }
    }
}


