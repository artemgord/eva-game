﻿using System;
using Source.Network.FlatProtocol;
using Source.Network.Protocol;

namespace Source.MessageFactory
{
    public static partial class MessageFactory
    {
        private static Guild CreateGuildMessage(FlatGuildObject flatGuild)
        {
            return new Guild(
                (GuildType) Enum.Parse(typeof(GuildType), flatGuild.Guild.ToString()),
                (byte) flatGuild.GuildLevel
            );
        }
    }
}