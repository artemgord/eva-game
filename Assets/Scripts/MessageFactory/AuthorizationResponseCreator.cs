﻿using System.Collections.Generic;
using Source.Network.FlatProtocol;
using Source.Network.Protocol;

namespace Source.MessageFactory
{
    public static partial class MessageFactory
    {
        private static AuthorizationResponse CreateAuthorizationResponseMessage(FlatMessage flatMessage)
        {
            FlatAuthorizationResponse? flatAuthorizationResponse = flatMessage.MessageObject<FlatAuthorizationResponse>();

            List<ShortPlayerMessage> userCharacters = new List<ShortPlayerMessage>();
            for (int i = 0; i < flatAuthorizationResponse.Value.UserCharsLength; ++i)
            {
                userCharacters.Add(CreateShortPlayerMessage(flatAuthorizationResponse.Value.UserChars(i).Value));
            }

            return new AuthorizationResponse(
                CreateMetadataMessage(flatAuthorizationResponse.Value.Metadata.Value),
                flatAuthorizationResponse.Value.Result,
                userCharacters
            );
        }
    }
}