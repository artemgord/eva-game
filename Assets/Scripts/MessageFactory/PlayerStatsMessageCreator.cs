﻿using Source.Network.FlatProtocol;
using Source.Network.Protocol;

namespace Source.MessageFactory
{
    public static partial class MessageFactory
    {
        private static PlayerStatsMessage CreatePlayerStatsMessage(FlatPlayerStatsMessage flatPlayerStatsMessage)
        {
            return new PlayerStatsMessage(
                CreateEUID(flatPlayerStatsMessage.PlayerID.Value),
                flatPlayerStatsMessage.Wins,
                flatPlayerStatsMessage.Losses,
                flatPlayerStatsMessage.Draw,
                flatPlayerStatsMessage.Escape,
                flatPlayerStatsMessage.WinsInRow
            );
        }
    }
}