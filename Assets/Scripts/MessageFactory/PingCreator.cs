using Source.Network.FlatProtocol;
using Source.Network.Protocol;

namespace Source.MessageFactory
{
    public static partial class MessageFactory
    {
        private static Ping CreatePingMessage(FlatMessage flatMessage)
        {
            FlatPing? flatPing = flatMessage.MessageObject<FlatPing>();

            return new Ping(flatPing.Value.Time);
        }
    }
}
