using System;
using Source.Network.FlatProtocol;
using Source.Network.Protocol;

namespace Source.MessageFactory
{
    public static partial class MessageFactory
    {
        public static Message CreateMessage(FlatMessage flatMessage)
        {
            switch (flatMessage.MessageObjectType)
            {
                case FlatMessageObject.FlatPing:
                    return CreatePingMessage(flatMessage);
                case FlatMessageObject.FlatPong:
                    return CreatePongMessage(flatMessage);
                case FlatMessageObject.FlatAuthorizationRequest:
                    return CreateAuthorizationRequestMessage(flatMessage);
                case FlatMessageObject.FlatAuthorizationResponse:
                    return CreateAuthorizationResponseMessage(flatMessage);
                case FlatMessageObject.FlatLoginRequest:
                    return CreateLoginRequest(flatMessage);
                case FlatMessageObject.FlatLoginResponse:
                    return CreateLoginResponse(flatMessage);
                default:
                    throw new ArgumentException("Unknown message", nameof(flatMessage));
            }
        }
    }
}
