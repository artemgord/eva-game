using Source.Network.FlatProtocol;
using Source.Network.Protocol;

namespace Source.MessageFactory
{
    public static partial class MessageFactory
    {
        private static Pong CreatePongMessage(FlatMessage flatMessage)
        {
            FlatPong? flatPong = flatMessage.MessageObject<FlatPong>();

            return new Pong(flatPong.Value.Time);
        }
    }
}
