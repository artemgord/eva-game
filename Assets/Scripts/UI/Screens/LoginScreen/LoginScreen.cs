﻿using System;
using System.Timers;
using Assets.Scripts.UI.Screens.LoginScreen;
using Source.Actors;
using Source.Contorllers.UI;
using Source.Network;
using Source.Network.Protocol;
using Source.Utilities;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Object = System.Object;

namespace Source.UI.Screens.LoginScreen
{
    public class LoginScreen : MonoBehaviour
    {
        public GameObject LoginPanel;
        public GameObject LoginField;
        public GameObject PasswordField;
        private string _login;
        private string _password;
        private LoginController _loginController;

        public LoginScreen()
        {
            _loginController = new LoginController();
        }

        private void Start()
        {
            new Initializer().InitializeInstances();
        }

        public void Login()
        {
            AuthorizationRequest request = new AuthorizationRequest(this._login, this._password);
            IFuture<AuthorizationResponse> future = Actor.Instance.ProceedRequest<AuthorizationResponse>(request, this.onResponse);
        }

        void Update()
        {
            this._login = this.LoginField.GetComponent<InputField>().text;
            this._password = this.PasswordField.GetComponent<InputField>().text;
        }

        void onResponse(AuthorizationResponse response)
        {
            Debug.Log("Successful!");
        }
    }
}
