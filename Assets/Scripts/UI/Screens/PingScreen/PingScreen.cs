﻿using Source.Network;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Source.Contorllers;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI.Screens.LoginScreen
{
    public class PingScreen : MonoBehaviour
    {
        public Text statisticTextField;
        private String statisticTextValue;
        private PingPongController pingPongController;

        void Start()
        {
            pingPongController = new PingPongController(this);
            pingPongController.Start();
        }

        void Update()
        {
            statisticTextField.text = statisticTextValue;
        }

        public void ShowPingStatistic(long ping, long delay)
        {
            statisticTextValue = "Ping: " + ping + "; Delay: " + delay;
        }
    }
}